/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Database;

import static Database.DBConnection.connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 *
 * @author TJ
 */
public class QueryController {
    private static String _query;
    private static Statement _statement;
    private static ResultSet _result;
    
    public static boolean makeQuery(String q){
        _query = q;
        
        try{
            _statement = connection.createStatement();
            if(_query.toLowerCase().startsWith("select")){
                _result = _statement.executeQuery(_query);
            }else{
                _statement.executeUpdate(_query);
            }
        }
        catch(SQLException ex){
            System.out.println("Error: " + ex.getMessage());
            return false;
        }
        return true;
    }
    public static ResultSet getResult(){
        return _result;
    }
    
    // Customer table queries
    public static ResultSet getCustomerList(){      
        String sqlStmt = "SELECT customerId \"id\", customerName \"name\","
             + " address , address2 address2, city , country, "
             + "postalCode \"zip\", phone\n" +
               "FROM customer c\n" +
               "JOIN address a ON (c.addressId = a.addressId)\n" +
               "JOIN city ci ON (a.cityId = ci.cityId)\n" +
               "JOIN country co ON (ci.countryId = co.countryId)\n" +
               "Order BY customerId;";
        
        makeQuery(sqlStmt);
        ResultSet results = getResult();
        
        return results;
    }
    public static boolean insertCustomer(String custName, String userName){
        String sqlStmt = "Insert into customer"
                + "(customerName, addressId, active, createDate, createdBy,"
                + " lastUpdate, lastUpdateBy)\n"
                +"values('"+ custName +"', (select max(addressId) from address),"
                + " 1, curdate(), '"+ userName + "', current_timestamp(),"
                + "'" + userName +"');";
        return makeQuery(sqlStmt);        
    }
    public static boolean  insertAddress
        (String address, String address2, int cityId, String phone,
         String zip, String userName){
            
            String sqlStmt = "INSERT INTO address(address, address2, cityId,"
                    + " postalCode, phone, createDate, createdBy, lastUpdate,"
                    + " lastUpdateBy)\n"
                    +"values('"+ address +"','"+ address2 + "',"
                    + cityId + ",'" + zip + "','" + phone + "', curdate(),"
                    + "'"+ userName +"', current_timestamp(),'"+ userName +"');";
            
            return makeQuery(sqlStmt);
        }
    public static boolean updateCustomer
        (int id, String custName, String address, String address2, String city, 
         String zip, String phone, String userName){
            
        String sqlStmt = 
                "Update customer, address\n" +
                "Set customerName = '" + custName + "',\n"+
                "    customer.lastUpdateBy = '" + userName + "',\n"+ 
                "    address.lastUpdateBy = '" + userName + "',\n"+
                "    address = '" + address + "',\n"+
                "    address2 = '" + address2 + "',\n"+
                "    cityId = "
                + "(SELECT cityId FROM city WHERE city = '"+ city + "'),\n"+
                "    postalCode = '" + zip + "',\n"+
                "    phone = '" + phone + "'\n"+
                "where customerId = " + id + "\n"+
                "and customer.addressId = address.addressId\n;";
        System.out.println(sqlStmt);
        
       return makeQuery(sqlStmt);       
    }
    public static boolean deleteCustomer(int id){
        String sqlStmt = "DELETE FROM customer\n"
                       + "WHERE customerId = " + id;
        
        return makeQuery(sqlStmt);
    }
    
    //Appointment table queries
    public static ResultSet getAppointmentList(String type){
        String sqlStmt;
        switch (type) {
            case "full":
                sqlStmt =
                        "SELECT appointmentId 'id', customerid, title, description"
                        + ", location, contact, type, url, start, end\n"
                        + "from appointment;";
                break;
            case "week":
                sqlStmt =
                        "select appointmentId 'id', customerId, title, description,\n" +
                        "location, contact, type, url, start, end\n" +
                        "from appointment\n" +
                        "where yearweek(start) = yearweek(curdate());";
                break;
            default:
                sqlStmt =
                        "select appointmentId 'id', customerId, title, description,\n" +            
                        "location, contact, type, url, start, end\n" +
                        "from appointment\n" +
                        "where monthname(start) = monthname(curdate());";
                break;
        }
        makeQuery(sqlStmt);
        ResultSet result = getResult();
        return result;
    }
    public static ResultSet getCusomterAppointmentList(String type, int id){
        String sqlStmt;
        switch (type) {
            case "full":
                sqlStmt =
                        "select appointmentId \"id\", customerId, title, "
                        + "description, location, contact, type, url,"
                        + " start, end, customerId  \n" +
                          "from appointment\n" +
                          "where customerId =" + id; 
                break;
            case "week":
                sqlStmt =
                    "select appointmentId 'id', customerId, title, description,\n" +
                    "location, contact, type, url, start, end\n" +
                    "from appointment\n" +
                    "where yearweek(start) = yearweek(curdate())\n" +
                    "AND customerId = " + id + ";";
                break;
            default:
                sqlStmt =
                    "select appointmentId 'id', customerId, title, description,\n" +
                    "location, contact, type, url, start, end\n" +
                    "from appointment\n" +
                    "where monthname(start) = monthname(curdate())\n" +
                    "AND customerId = " + id + ";";
                break;
        }
        makeQuery(sqlStmt);
        ResultSet result = getResult();
        return result;        
    }
    public static boolean insertAppointment
        (int custId, String title, String description, String location,
         String contact, String url, ZonedDateTime start, ZonedDateTime end,
         String userName, String type, int userid){
            
            String sqlStmt = "insert into appointment"
                    + "(customerId, title, description, location, contact, url,"
                    + " start, end, createDate, createdBy, lastUpdate, lastUpdateBy,"
                    + " type, userId)\n" +
                    "values(" + custId +", '"+ title + "', '"
                    + description + "', '" + location + "', '" + contact+ "', '"
                    + url + "', '" + start.toLocalDateTime() + "',"
                    + " '" + end.toLocalDateTime() + "'"
                    + ",curdate(), " + "'"+ userName+ "',"
                    + " current_timestamp(),"
                    + "'"+ userName + "','" +type +"',"
                    + "" + userid+");" ;
            
           return makeQuery(sqlStmt);              
        }
    public static boolean updateAppointment
        (int apppointmentId, String title, String description, String location,
         String contact, String url, ZonedDateTime start, ZonedDateTime end,
         String userName, String type, int userid){
            
            String sqlStmt =
                "Update appointment\n" +
                "set title = '" + title + "',\n" +
                "description = '" + description + "',\n" +
                "location = '" + location + "',\n" +
                "contact = '" + contact + "',\n" +
                "url = '" + url + "',\n" +
                "start = '" + start.toLocalDateTime() + "',\n" +
                "end = '" + end.toLocalDateTime() + "',\n" +
                "lastUpdateBy = '" + userName +"',\n" +
                "type = '" + type + "',\n" +
                "userId = "+ userid +"\n"+
                "where appointmentId = " + apppointmentId +";";
            
            return makeQuery(sqlStmt);
        } 
    public static boolean deleteAppointment(int id){
        String sqlStmt = "DELETE FROM appointment\n"
                       + "WHERE appointmentId = " + id + ";";
        return makeQuery(sqlStmt);
    }
    public static boolean deleteCustomerAppointments(int custId){
        String sqlStmt = "DELETE FROM appointment\n"+
                         "WHERE customerId = " + custId + ";";
        return makeQuery(sqlStmt);
    }
    public static ResultSet getAppointmentTime(LocalDate date, String contact){
        String sqlStmt = 
                "SELECT start, end\n"
              + "FROM appointment\n"
              + "WHERE contact = '" + contact + "'\n"
              + "AND date(start) = '" + date + "'\n"
              + "ORDER BY start;";
        QueryController.makeQuery(sqlStmt);
        return getResult();
    }
    
    // User table queries
    public static ResultSet login(String name, String password){
        String sqlStmt = 
                "Select strcmp("
              + "(Select password "
              + "from user where userName = '" + name +"'),'"+password+"') 'verify',"
                + "userId\n"+
                "from user\n"+
                "where userName = '" + name +"';" ;
        makeQuery(sqlStmt);
        ResultSet result = getResult();
        
        return result;
    }
    public static ResultSet checkAppointments(int id){
        String sqlStmt = 
                "SELECT customerName 'name', description, start \n" +
                "FROM appointment a\n" +
                "JOIN customer c ON (a.customerId = c.customerId)\n" +
                "WHERE userId = " + id + "\n" +
                "AND date(start) = curdate();";
        makeQuery(sqlStmt);
        return getResult();
    }
    public static ResultSet userList(){
        String sqlStmt = "SELECT userId, userName FROM user;";
        makeQuery(sqlStmt);
        return getResult();
    }
    
    // Reports 
    public static ResultSet appointmentsPerMonth(){
        String sqlStmt = 
                "SELECT monthname(start) 'month', count(month(start)) 'count'\n" +
                "FROM appointment\n" +
                "WHERE year(start) = year(curdate())"+
                "GROUP BY monthname(start);";
        
        makeQuery(sqlStmt);
        return getResult();
    }
    public static ResultSet userAppointments(int id){
        String sqlStmt = 
                "SELECT customerName, type, start, end\n"+
                "FROM customer c "+
                "JOIN appointment a ON (c.customerId = a.customerId)"+
                "WHERE userId = " + id +";";
        
        makeQuery(sqlStmt);
        return getResult();
    }
    public static ResultSet customerPerCity(){
        String sqlStmt = 
                "select city, count(a.cityId)\n" +
                "from city c\n" +
                "join address a on (c.cityId = a.cityId)\n" +
                "group by city;";
        
        makeQuery(sqlStmt);
        return getResult();
    }
             
             
}
