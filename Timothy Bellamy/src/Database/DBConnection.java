/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author TJ
 */
public class DBConnection {
    private static final String NAME = "U05Lqz";
    private static final String DB_URL = "jdbc:mysql://52.206.157.109/" + NAME;
    private static final String USERNAME = "U05Lqz";
    private static final String PWD = "53688535805";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    static Connection connection;
    
    public static void makeConnection()
            throws ClassNotFoundException, SQLException, Exception{
        
        Class.forName(DRIVER);
        connection = DriverManager.getConnection(DB_URL, USERNAME, PWD);
        System.out.println("Connection Successful");
    }
    public static void closeConnection()
            throws ClassNotFoundException, SQLException, Exception{
        
        connection.close();
        System.out.println("Connection closed");
    }
    
}
