/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class AddCustomerController {
    
    private Controller _controller = Controller.getController();

    @FXML
    private Button save, cancel;
    @FXML
    private TextField name, address, address2, zip, phone;
    @FXML
    private ComboBox<String> cityList, countryList;
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        // TODO
        cityList.getItems().setAll(_controller.getCityList());
        countryList.getItems().setAll(_controller.getCountryList(""));
        
    }
    @FXML
    private void onClickSelectCity(){
        String item = cityList.getValue();
        countryList.getItems().setAll(_controller.getCountryList(item));
    }
    @FXML
    private void onClickSave() throws IOException, SQLException{
        String cName = name.getText();
        String cAddress = address.getText();
        String cAddress2 = address2.getText();
        String cZip = zip.getText();
        String cPhone = phone.getText();
        String cCity = cityList.getSelectionModel().getSelectedItem();
        String cCountry = countryList.getSelectionModel().getSelectedItem();
        
        _controller.addCustomer
            (cName, cAddress, cAddress2, cCity, cCountry, cZip, cPhone);
        
        
       returnHome();
        
        
    }
    @FXML
    private void onClickCancel() throws IOException{
        returnHome();
    }
    private void returnHome() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) cancel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
}
