/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Platform.exit;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class LoginController{
    
    private ResourceBundle rb;
    private Locale locale;

    private Controller _controler = Controller.getController();
    @FXML
    private TextField userName, password;
    @FXML
    private Button enter, cancel;
    @FXML
    private Label login, user, passwordLabel;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        // TODO
        locale = Locale.getDefault();
        
        rb = ResourceBundle.getBundle("appointment-scheduler", locale);
        login.setText(rb.getString("Login"));
        user.setText(rb.getString("User"));
        enter.setText(rb.getString("Enter"));
        cancel.setText(rb.getString("Cancel"));
        passwordLabel.setText(rb.getString("Password"));
        userName.setPromptText(rb.getString("PromptName"));
        password.setPromptText(rb.getString("PromptPassword"));
        
        
    }   
    @FXML
    private void onClickEnter() throws IOException{
        String name = userName.getText();
        String pwd = password.getText();
        
        int valid = _controler.login(name, pwd);
        Alert alert = new Alert(Alert.AlertType.ERROR);
        ButtonType ok = new ButtonType(rb.getString("Ok"));
        
        switch (valid) {
            case 1:
                alert.setTitle(rb.getString("TitlePassword"));
                alert.setHeaderText(rb.getString("HeaderPassword")+ " " + name);
                alert.getButtonTypes().setAll(ok);
                alert.showAndWait();
                break;
            case -1:
                alert.setTitle(rb.getString("TitleUser"));
                alert.setHeaderText(rb.getString("HeaderUser"));
                alert.getButtonTypes().setAll(ok);
                alert.showAndWait();
                break;                
            case -2:
                alert.setTitle(rb.getString("TitleData"));
                alert.setHeaderText(rb.getString("HeaderData"));
                alert.getButtonTypes().setAll(ok);
                alert.showAndWait();
                break;
            default:
        {
            try {
                checkUpcomingAppointment();
            } catch (SQLException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
                break;
        }
    }
    @FXML
    private void onClickCancel(){
        exit();  
    }
    private void checkUpcomingAppointment() throws SQLException, IOException{
        ArrayList appointment = _controler.checkAppointments();
        ButtonType ok = new ButtonType(rb.getString("Ok"));
       if(appointment != null){
           Alert alert = new Alert(Alert.AlertType.INFORMATION);
           alert.setTitle(rb.getString("TitleApp"));
           alert.setHeaderText(
                   rb.getString("HeaderApp") +" " + appointment.get(0)+
                   " " + rb.getString("At") + " " + appointment.get(2) + ".");
           alert.setContentText(
                   rb.getString("Detail") +"\n" +
                   rb.getString("Customer")+" " + appointment.get(0) + "\n" + 
                   rb.getString("Description")+" " + appointment.get(1) + "\n" + 
                   rb.getString("Start")+" " + appointment.get(2) + "\n");
           alert.showAndWait();
       }
       homeScreen();
    }
    private void homeScreen() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) cancel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();      
    }
    
}
