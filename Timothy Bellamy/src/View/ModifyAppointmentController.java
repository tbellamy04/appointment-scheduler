/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import Model.Customer;
import Model.User;
import java.io.IOException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class ModifyAppointmentController{
    
    private Controller _controller = Controller.getController();
    
    @FXML
    private Button cancel;
    @FXML
    private TextField title, description, url, id;
    @FXML
    private ComboBox<String> office, time,type;
    @FXML
    private ComboBox<Integer> length;
    @FXML
    private ComboBox<Customer> customer;
    @FXML
    private ComboBox<User> contact;
    @FXML
    private DatePicker date;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
        
        office.getItems().setAll(_controller.getLocationList());
        contact.getItems().setAll(_controller.userList);
        customer.getItems().setAll(_controller.customerList); 
        type.getItems().setAll(_controller.getType());
   
        id.setText(Integer.toString(_controller.getAppointment().getId()));
        customer.getSelectionModel().select(_controller.getCustomer());
        title.setText(_controller.getAppointment().getTitle());
        description.setText(_controller.getAppointment().getDescription());
        office.getSelectionModel().select(_controller.getAppointment().getLocation());
        contact.getSelectionModel().select(_controller.getUserByName(_controller.getAppointment().getContact()));
        type.getSelectionModel().select(_controller.getAppointment().getType());
        url.setText(_controller.getAppointment().getUrl());
        date.setValue(_controller.getAppointment().getDate());
        time.getSelectionModel().select(_controller.getAppointment().getStart().format(format));
        length.getSelectionModel().select(_controller.getLength());        
    }
    @FXML
    private void onClickSave() throws IOException{
        int aId = Integer.parseInt(id.getText());
        DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime local = LocalTime.parse(time.getValue(), format);
        _controller.modifyAppointment(
                    aId,
                    title.getText(),
                    description.getText(),
                    office.getValue(),
                    contact.getValue(),
                    type.getValue(),
                    url.getText(), 
                    ZonedDateTime.of(date.getValue(), local, ZoneId.systemDefault()),
                    length.getValue());
        returnHome();
    }
    @FXML
    private void onClickCancel() throws IOException{
        returnHome();
    }
    @FXML
    private void onSelectDate(){
         time.getItems().setAll(_controller.getAvailableTimes
            (date.getValue(), contact.getValue()));       
    }
    @FXML
    private void onSelectTime(){
        length.getItems().setAll
            (_controller.getAvailableLength(time.getValue(), time.getItems()));     
    }
    
    // Inner Helper methods
    private void returnHome() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) cancel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    
}
