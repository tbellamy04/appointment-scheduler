/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import Model.Appointment;
import Model.Customer;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.Optional;
import static javafx.application.Platform.exit;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class MainScreenController{
    private Controller _controller = Controller.getController();
    
    @FXML
    private ComboBox<Customer> custList = new ComboBox();
    @FXML
    private TableView<Appointment> appTable;
    @FXML
    private TableColumn<Appointment, String> title, description, contact, start, end;
    @FXML
    private TableColumn<Appointment, Date> date;
    @FXML
    private Button addCustomer, deleteCustomer, exit, modifyCustomer, addAppointment,
                   modifyAppointment, login, report;
    @FXML 
    private MenuItem close, about;
    @FXML
    private Label welcome;
    @FXML
    private ToggleButton allAppointments, appointmentWeek, appointmentMonth;
    
    @FXML
    private ToggleGroup appointmentFilter;
    
    
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        // TODO
        custList.getItems().setAll(_controller.customerList); 
        // Loads all appointments by default
        onClickShowAll();
        
        title.setCellValueFactory(cellData ->
            cellData.getValue().getTitleProperty());
        description.setCellValueFactory(cellData ->
            cellData.getValue().getDescriptionProperty());
        date.setCellValueFactory(cellData ->
            cellData.getValue().getDateProperty());
        start.setCellValueFactory(cellData ->
            cellData.getValue().getStartProperty());
        end.setCellValueFactory(cellData ->
            cellData.getValue().getEndProperty());
        contact.setCellValueFactory(cellData ->
            cellData.getValue().getContactProperty());
        appTable.setItems(_controller.appointmentlist);
        
        
        
        if(_controller.getUser() != null){
            setWelcome();
        }else{
            welcome.setText("Welcome");
        }
    }
    @FXML
    private void onSelectCustomer(){
        if(appointmentWeek.isSelected()){
            _controller.setCustomer(custList.getValue());
            onClickShowWeek();
        }else if(appointmentMonth.isSelected()){
            _controller.setCustomer(custList.getValue());
            onClickShowMonth();
        }else{
            _controller.setCustomer(custList.getValue());
            onClickShowAll();
        }
        
        
    }
    @FXML
    private void setWelcome(){
        welcome.setText("Welcome " + _controller.getUser().getUserName());
    }
    @FXML
    private void onClickExit(){
        exit();
    }
    
    @FXML
    // Customer Controls
    private void onClickAddCustomer() throws IOException{
        newScene("addCustomer.fxml", addCustomer);
    }
    @FXML
    private void onClickModifyCustomer() throws IOException{
        Alert alert = new Alert(Alert.AlertType.ERROR);
        
        if(custList.getSelectionModel().getSelectedItem() == null){
            alert.setTitle("Invalid Customer");
            alert.setHeaderText("Invalid customer\n"
                              + "Please select a customer to be modify");
            alert.showAndWait();
        }else{
            newScene("modifyCustomer.fxml", modifyCustomer);
        }
    }
    @FXML
    private void onClickDeleteCustomer(){
        if(custList.getSelectionModel().getSelectedItem() != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Confirmation");
            alert.setHeaderText("Deleting Customer: " + _controller.getCustomer());
            alert.setContentText("Deleting this customer will also remove all appointments.");
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                /** Due to database FK constraint delete all appointments matching 
                 *  selected customer.
                 **/
                if(_controller.appointmentlist.size() > 0){
                    _controller.removeAppointment("All");
                }
                // Remove customer and reload custList into combobox 
                _controller.removeCustomer();
                custList.getItems().setAll(_controller.customerList);
                custList.getSelectionModel().selectFirst();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Customer");
            alert.setHeaderText("Invalid Customer\n"
                    + "Please select a customer to delete");
            alert.showAndWait();   
        }
    }
    
    // Appointment Controls
    @FXML
    private void onClickAddAppointment() throws IOException{
        newScene("addAppointment.fxml", addAppointment);
    }
    @FXML
    private void onClickModifyAppointment() throws IOException{
        Alert alert = new Alert(Alert.AlertType.ERROR);
        
        if(_controller.getCustomer() == null){
            alert.setTitle("Invalid Customer");
            alert.setHeaderText("No customer selected. Please select a cutomer and try again");
            alert.showAndWait();
        }else if(appTable.getSelectionModel().getSelectedItem() == null){
            alert.setTitle("Invlaid Appointment");
            alert.setHeaderText("Invalid Appointment\n"
                    + "Please select an appointment to modify");
            alert.showAndWait();                 
        }else{
            _controller.setAppointment(appTable.getSelectionModel().getSelectedItem());
           newScene("modifyAppointment.fxml", modifyAppointment); 
        }
        
    }
    @FXML
    private void onClickAppointmentDelete(){
        _controller.setAppointment(appTable.getSelectionModel().getSelectedItem());
        
        if(_controller.getAppointment() != null){
        _controller.removeAppointment("Single");
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Appointment");
            alert.setHeaderText("Invalid Appointment\n"
                    + "Please select an appointment to delete");
            alert.showAndWait();           
        }
    }
    @FXML
    private void onClickShowWeek(){
         _controller.createAppointmentList("week");
    }
    @FXML
    private void onClickShowAll(){
         _controller.createAppointmentList("full");

    }
    @FXML 
    private void onClickShowMonth(){
        _controller.createAppointmentList("month");
    }
    
    // User Controls
    @FXML
    private void onClickLogin() throws IOException{
        newScene("login.fxml", login);
        
    }
    // Report Controls
    @FXML
    private void onClickReport() throws IOException{
        newScene("reports.fxml", report);
    }
    // Inner main screen scene handler
    private void newScene(String file, Button button) throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) button.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(file));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();        
    }

}
