/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import Model.Customer;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class AddAppointmentController{

    private Controller _controller = Controller.getController();
    
    @FXML
    private Button cancel;
    @FXML
    private TextField title, description, url;
    @FXML
    private ComboBox<String> office, time, type;
    @FXML
    private ComboBox<Integer> length;
    @FXML
    private ComboBox<Customer> customer;
    @FXML
    private ComboBox<User> contact;
    @FXML
    private DatePicker date;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        // TODO
        office.getItems().setAll(_controller.getLocationList());
        contact.getItems().setAll(_controller.userList);
        customer.getItems().setAll(_controller.customerList);
        type.getItems().setAll(_controller.getType());
        

    } 
    
    @FXML
    private void onClickCancel() throws IOException{
        returnHome();
    }
    @FXML
    private void onClickSave() throws IOException, SQLException{
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime local = LocalTime.parse(time.getValue(), format);
        
        _controller.addAppointment(
                customer.getValue(),
                title.getText(),
                description.getText(),
                office.getValue(),
                contact.getValue(),
                type.getValue(),
                url.getText(),
                ZonedDateTime.of(date.getValue(), local, ZoneId.systemDefault()),
                length.getValue());
        
        
        returnHome();
    }
    @FXML
    private void onSelectDate(){
        time.getItems().setAll(_controller.
                getAvailableTimes(date.getValue(), contact.getValue()));
        

    }
    @FXML
    private void onSelectTime(){
        length.getItems().setAll
            (_controller.getAvailableLength(time.getValue(), time.getItems()));
        
    }
    @FXML
    private void onSelectLength(){
    }
    
    // Inner Helper methods
    private void returnHome() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) cancel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
}



