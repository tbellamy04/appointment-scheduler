/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author TJ
 */
public class ModifyCustomerController{

    /**
     * Initializes the controller class.
     */
    // Get controller instance
    Controller _controller = Controller.getController();
    
    // Set all GUI fields
    @FXML
    private TextField id, name, address, address2, zip, phone;
    @FXML
    private ComboBox<String> cityList, countryList;
    @FXML
    private Button save, cancel;
    
    @FXML
    public void initialize() {
        id.setText(Integer.toString(_controller.getCustomer().getId()));
        name.setText(_controller.getCustomer().getName());
        address.setText(_controller.getCustomer().getAddress());
        address2.setText(_controller.getCustomer().getAddress2());
        cityList.getItems().setAll(_controller.getCityList());
        cityList.getSelectionModel().select(_controller.getCustomer().getCity());
        countryList.getItems().setAll(_controller.getCountryList(""));
        countryList.getSelectionModel().select(_controller.getCustomer().getCountry());
        zip.setText(_controller.getCustomer().getZip());
        phone.setText(_controller.getCustomer().getPhone());

    }
    
    @FXML
    private void onClickSelectCity(){
        String item = (String) cityList.getValue();
        countryList.getItems().setAll(_controller.getCountryList(item));
    }
    @FXML
    private void onClickSave() throws IOException{
        int cId = Integer.parseInt(id.getText());
        String cName = name.getText();
        String cAddress = address.getText();
        String cAddress2 = address2.getText();
        String cZip = zip.getText();
        String cPhone = phone.getText();
        String cCity = cityList.getSelectionModel().getSelectedItem();
        String cCountry = countryList.getSelectionModel().getSelectedItem();     
        
        _controller.modifyCustomer(cId, cName, cAddress, cAddress2, cCity, cCountry, cZip, cPhone);
        
        returnHome();
    }
    @FXML
    private void onClickCancel() throws IOException{
        returnHome();
    }
    private void returnHome() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) cancel.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();        
    }
    
}
