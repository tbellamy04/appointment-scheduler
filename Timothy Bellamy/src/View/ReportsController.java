/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package View;

import Controller.Controller;
import Model.Reports;
import Model.User;
import java.io.IOException;
import java.sql.SQLException;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author TJ
 */
public class ReportsController{
    
    private Controller _controller = Controller.getController();
    
    @FXML 
    private TableView reportTable;
    @FXML
    private Button exit;
    @FXML
    private ComboBox<User> user;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        // TODO
        user.getItems().setAll(_controller.userList);
    }    
    @FXML
    private void onClickMonth() throws SQLException{
        reportTable.getItems().clear();
        reportTable.getColumns().clear();
        TableColumn<Reports, String> month = new TableColumn("Month");
        TableColumn<Reports, String> count = new TableColumn("Appointments");
        
        reportTable.getColumns().addAll(month, count);
        
        ObservableList<Reports> report = _controller.appointmentsPerMonth();
        
        month.setCellValueFactory(cellData ->
            cellData.getValue().getMonth());
        count.setCellValueFactory(cellData ->
            cellData.getValue().getAppointmentCount()); 
        reportTable.setItems(report);
        
    }
    @FXML
    private void onClickSchedule(){
        reportTable.getItems().clear();
        reportTable.getColumns().clear();     
        
        ObservableList<Reports> report = _controller.getUserAppointments(user.getValue());
        
        TableColumn<Reports, String> name = new TableColumn("Customer");
        TableColumn<Reports, String> type = new TableColumn("Meeting Type");
        TableColumn<Reports, String> date = new TableColumn("Date");
        TableColumn<Reports, String> start = new TableColumn("Start Time");
        TableColumn<Reports, String> end = new TableColumn("End Time");
        
        reportTable.getColumns().addAll(name, type, date, start, end);
        
        name.setCellValueFactory(cellData ->
            cellData.getValue().getCustomerName());
        type.setCellValueFactory(cellData ->
            cellData.getValue().getType()); 
        date.setCellValueFactory(cellData ->
            cellData.getValue().getDate());
        start.setCellValueFactory(cellData ->
            cellData.getValue().getStart()); 
        end.setCellValueFactory(cellData ->
            cellData.getValue().getEnd()); 
        reportTable.setItems(report);
        
        
        
    }
    @FXML
    private void onClickCity(){
        reportTable.getItems().clear();
        reportTable.getColumns().clear();     
        
        ObservableList<Reports> report = _controller.getCustomerCityCount();
        
        TableColumn<Reports, String> city = new TableColumn("City");
        TableColumn<Reports, String> count = new TableColumn("Customer Count");
       
        reportTable.getColumns().addAll(city, count);
        
        city.setCellValueFactory(cellData ->
            cellData.getValue().getCity());
        count.setCellValueFactory(cellData ->
            cellData.getValue().getCityCount()); 
        reportTable.setItems(report);
        
        
    }
    @FXML
    private void onClickExit() throws IOException{
        Stage stage;
        Parent root;
        
        stage =(Stage) exit.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("mainScreen.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();            
    }
}
