/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Controller;

import Database.QueryController;
import Model.Appointment;
import Model.Customer;
import Model.Reports;
import Model.User;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import static java.time.temporal.ChronoUnit.MINUTES;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author TJ
 */
public class Controller {
    
    private static final Controller CONTROLLER = new Controller();
    
    
    public ObservableList<Customer> customerList;
    public ObservableList<Appointment> appointmentlist;
    public ObservableList<User> userList;
    private Customer _customer;
    private Appointment _selectedAppointment;
    private User _user;
    private String dataBaseZone = "Europe/London";

    
    
    private Controller(){

        // Create customer list
        createCustomerList();
        createUserList();
        appointmentlist = FXCollections.observableArrayList();


    }
    public static Controller getController(){
        return CONTROLLER;
    }
    // Inner Controller class helper methods
    private void createCustomerList(){
        
        ResultSet results = QueryController.getCustomerList();
        
        try{
            customerList = FXCollections.observableArrayList();
            while(results.next()){
                customerList.add(new Customer
               (results.getInt("id"), 
                results.getString("name"),
                results.getString("address"),
                results.getString("address2"),
                results.getString("city"),
                results.getString("country"),
                results.getString("zip"),
                results.getString("phone")));
            }
        }catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
    }
    private void createUserList(){
        try {
            ResultSet result = QueryController.userList();
           userList = FXCollections.observableArrayList(); 
            while(result.next()){
                
                userList.add(new User(
                        result.getInt(1),
                        result.getString(2)
                ));
            }
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        }
        
    }

    private int getNewCustomerId() throws SQLException{
        String sqlStmt = "SELECT max(customerId) FROM customer;";
        QueryController.makeQuery(sqlStmt);
        ResultSet result = QueryController.getResult();
        int id = 0;
        while(result.next()){
            id = result.getInt("max(customerId)")+1;
        }
        return id;
    }
    private int getNewAppointmentId() throws SQLException{
        String sqlStmt = "SELECT MAX(appointmentId) 'id' FROM appointment;";
        QueryController.makeQuery(sqlStmt);
        ResultSet result = QueryController.getResult();
        
        int id = 0;
        while(result.next()){
            id = result.getInt("id");
        }
        return id;
    }
    
    // Data manipulation methods
    // Customer methods
    public void addCustomer
        (String name, String address, String address2, String city,
        String country, String zip, String phone) throws SQLException{
            
            int id = getNewCustomerId();
            Customer customer = new Customer
            (id, name, address, address2, city, country, zip, phone);
            
            customerList.add(customer);
            QueryController.insertAddress
                (address, address2, id, phone, zip, _user.getUserName());
            QueryController.insertCustomer(name, _user.getUserName());
        }
    public void modifyCustomer
        (int id, String name, String address, String address2, String city,
        String country, String zip, String phone){
            for(int i = 0; i < customerList.size(); i++){
                if(customerList.get(i).getId() == id){
                    // Update customer list
                    customerList.get(i).setName(name);
                    customerList.get(i).setAddress(address);
                    customerList.get(i).setAddress2(address2);
                    customerList.get(i).setCity(city);
                    customerList.get(i).setCountry(country);
                    customerList.get(i).setZip(zip);
                    customerList.get(i).setPhone(phone);
                }
            }
            // Update database
            QueryController.updateCustomer
                (id, name, address, address2, city, zip, phone, _user.getUserName());
        }
    public void removeCustomer(){
        // Remove from Database
        QueryController.deleteCustomer(_customer.getId());
        // Remove from local customer list
        customerList.remove(_customer);
    }
    public void setCustomer(Customer customer){
        _customer = customer;
    }
    public Customer getCustomer(){
        return _customer;
    }
    
    // Appointment methods
    public void setAppointment(Appointment appointment){
        _selectedAppointment = appointment;
    }
    public Appointment getAppointment(){
        return _selectedAppointment;
    }
    public void createAppointmentList(String type){
        // Checks wether there has been a cutomer selected and loads appropriate appointmentlist
        ResultSet result;
        if(_customer == null){
            result = QueryController.getAppointmentList(type);
        }else{
            result = QueryController.getCusomterAppointmentList(type, _customer.getId());
        }
        
          try{
            appointmentlist.clear();
            while(result.next()){
               
                // Sets time to the database's time zone
                ZonedDateTime start = ZonedDateTime.
                        of(result.getTimestamp("start").toLocalDateTime(), ZoneId.of(dataBaseZone));
                ZonedDateTime end = ZonedDateTime.
                        of(result.getTimestamp("end").toLocalDateTime(), ZoneId.of(dataBaseZone));
                appointmentlist.add(new Appointment(
                        result.getInt("id"),
                        result.getInt("customerId"),
                        result.getString("title"),
                        result.getString("description"),
                        result.getString("location"),
                        result.getString("contact"),
                        result.getString("type"),
                        result.getString("url"),
                        // Converts to local time before storing
                        start.withZoneSameInstant(ZoneId.systemDefault()), 
                        end.withZoneSameInstant(ZoneId.systemDefault())));
            }
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }    
    }

    public void addAppointment
        (Customer customer,  String title, String description,
         String location,User contact, String type, String url, 
         ZonedDateTime start, int length) throws SQLException{
            
            int custId = customer.getId();
            int id = getNewAppointmentId();
            ZonedDateTime end = start.plusMinutes(length);
            
            int userId;
            
            
                      
            
            Appointment appointment = new Appointment
            (id, custId, title, description, location, contact.getUserName(),
                    type, url,start, end);
            
            appointmentlist.add(appointment);
            
            // Convert time back to database time zone before updating
            QueryController.insertAppointment
                (custId, title, description, location, contact.getUserName(), url, 
                 start.withZoneSameInstant(ZoneId.of(dataBaseZone)),
                 end.withZoneSameInstant(ZoneId.of(dataBaseZone)),
                 _user.getUserName(), type, contact.getUserId());
    }
    public void modifyAppointment
         (int id, String title, String description, String location,
         User contact, String type, String url, ZonedDateTime start, int length){
           

            ZonedDateTime end = start.plusMinutes(length);
            
            _selectedAppointment.setTitle(title);
            _selectedAppointment.setDescription(description);
            _selectedAppointment.setLocation(location);
            _selectedAppointment.setContact(contact.getUserName());
            _selectedAppointment.setType(type);
            _selectedAppointment.setUrl(url);
            _selectedAppointment.setStart(start);
            _selectedAppointment.setEnd(end);
            
            // Save to database
            QueryController.updateAppointment
                (id, title, description, location,
                 contact.getUserName(), url, 
                 start.withZoneSameInstant(ZoneId.of(dataBaseZone)),
                 end.withZoneSameInstant(ZoneId.of(dataBaseZone))
                 , contact.getUserName(), type, contact.getUserId() );
            
         }
    public void removeAppointment(String type){
        // If type is single remove only the selected appointment else remove
        // all appointments assigned to the customer
        if(type.toLowerCase().equals("single")){
            // Remove from database
           QueryController.deleteAppointment(_selectedAppointment.getId());
           // Remove form local Appointment list
           appointmentlist.remove(_selectedAppointment);
        }else{
            QueryController.deleteCustomerAppointments(_customer.getId());
            appointmentlist.clear();
        }
    }
    public Integer getLength(){
        // Returns length of meeting for modify appointment scree
        long length = MINUTES.between(_selectedAppointment.getStart(), _selectedAppointment.getEnd());
        
        
        return Math.toIntExact(length);
    }
    
   // User methods
    public int login(String name, String password){
        
        ResultSet result = QueryController.login(name, password);
        
        /*
            validUser -1 = Invalid userName entered
            validUser 1 = Invalid password entered
            validUser -2 = Unable to connect to database
        */
        int validUser = -1;
        int userId = 0;
        try{
            while(result.next()){
                validUser = result.getInt("verify");
                userId = result.getInt("userId");
            }
        }catch(SQLException e){
            validUser = -2;
        }
        
        if(validUser == 0){
            
            _user = new User(userId, name);
            
   
            ExecutorService service = null;
            try{
                service = Executors.newSingleThreadExecutor();
                service.execute(() -> recordLogin(ZonedDateTime.now()));
            }finally{
                if(service != null) service.shutdown();
            }
      
        }
        return validUser;
    }
    private void recordLogin(ZonedDateTime stamp){
       if(! Files.exists(Paths.get("activity"))){
            try{
                 Files.createDirectory(Paths.get("activity"));
            }catch(IOException e){
                System.out.println("Error: File already exits");
            }
       }
       DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss z");
       if(Files.exists(Paths.get("activity/user_logins.txt"))){
           Path path = Paths.get("activity/user_logins.txt");
           
           try(BufferedWriter writer = Files.newBufferedWriter(path,
                   Charset.forName("US-ASCII"), StandardOpenOption.APPEND)){
               writer.write(_user.getUserName() + " " + stamp.format(format) + "\r\n");
           }catch(IOException e){
               System.out.println("Error: " + e.getMessage());
           }        
       }else{
           Path path = Paths.get("activity/user_logins.txt");
           try(BufferedWriter writer = Files.newBufferedWriter(path,
                   Charset.forName("US-ASCII"))){
               writer.write("User\tDate\t   Time\r\n" +
                       "----------------------------------\r\n"+
                       _user.getUserName() + "\t" + stamp.format(format)+ "\r\n");
           }catch(IOException e){
               System.out.println("Error: " + e.getMessage());
           }               
       }
 
    }
    public ArrayList checkAppointments() throws SQLException{
        ResultSet result = QueryController.checkAppointments(_user.getUserId());
        
        BiFunction<ZonedDateTime, ZonedDateTime, Long> minBetween = MINUTES::between;
        Predicate<Long> minLen =  m -> m > 0 && m <= 15;
        while(result.next()){
            ZonedDateTime start = 
                    ZonedDateTime.of(result.getTimestamp("start").toLocalDateTime(), ZoneId.of(dataBaseZone));
            
            start = start.withZoneSameInstant(ZoneId.systemDefault());
            
            Long min = minBetween.apply(ZonedDateTime.now(),start);
            if(minLen.test(min)){
                DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a z");
                ArrayList<String> appointment = new ArrayList<>();
                appointment.add(result.getString("name"));
                appointment.add(result.getString("description"));
                appointment.add(start.format(format));
                return appointment;
            }
        }
        return null;
    }
    public User getUser(){
        return _user;
    }
    public User getUserByName(String name){
        for(User e : userList){
            if(e.getUserName().equals(name)){
                return e;
            }
        }
        return null;
    }
    
    // Reports Methods
    public ObservableList appointmentsPerMonth() throws SQLException{
        ObservableList<Reports> report = FXCollections.observableArrayList();
        ResultSet result = QueryController.appointmentsPerMonth();
        
        while(result.next()){
            Reports r = new Reports();
            r.setMonth(result.getString("month"));
            r.setAppointmentCount(result.getString("count"));
            report.add(r);
        }
        return report;
    }
    public ObservableList getUserAppointments(User user){
        ObservableList<Reports> report = FXCollections.observableArrayList();
        try {
            ResultSet result = QueryController.userAppointments(user.getUserId());
            
            while(result.next()){
                ZonedDateTime start =
                        ZonedDateTime.of(result.getTimestamp(3).toLocalDateTime(), ZoneId.of(dataBaseZone));
                ZonedDateTime end =
                        ZonedDateTime.of(result.getTimestamp(4).toLocalDateTime(), ZoneId.of(dataBaseZone));
                
                start = start.withZoneSameInstant(ZoneId.systemDefault());
                end =  end.withZoneSameInstant(ZoneId.systemDefault());
                DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
                
                Reports r = new Reports();
                
                r.setCustomerName(result.getString(1));
                r.setType(result.getString(2));
                r.setStart(start.format(format));
                r.setEnd(end.format(format));
                r.setDate(start.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
                
                report.add(r);
            }
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        }
        return report;
    }
    public ObservableList getCustomerCityCount(){
        ObservableList<Reports> report = FXCollections.observableArrayList();
        
        ResultSet result = QueryController.customerPerCity();
        
        try {
            while(result.next()){
                Reports r = new Reports();
                
                r.setCity(result.getString(1));
                r.setCityCount(result.getString(2));
                
                report.add(r);
            }
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        }
        
        
        return report;           
    }
    
    // Menu list methods
    public ObservableList getCityList(){
        ObservableList<String> list;
        
        String sqlStmt = "SELECT city From city;";
        QueryController.makeQuery(sqlStmt);
        ResultSet result = QueryController.getResult();
        
        try{
            list = FXCollections.observableArrayList();
            while(result.next()){
                list.add(result.getString("city"));
            }
            return list;
            
        }catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
    public ObservableList getCountryList(String city){
        ObservableList<String> list;
        String sqlStmt;
        
        if(city.equals("")){
            sqlStmt = "SELECT country FROM country;";
        }else{
            sqlStmt = "SELECT country\n" +
                      "from city\n" +
                      "join country "
                    + "on (city.countryId = country.countryId) \n" +
                      "where city = " + "\""+ city + "\";";
        }
        QueryController.makeQuery(sqlStmt);
        ResultSet result = QueryController.getResult();
        
        try{
            list = FXCollections.observableArrayList();
            while(result.next()){
                list.add(result.getString("country"));
            }
            return list;
            
        }catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }
    public ObservableList getLocationList(){
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll("New York, New York", "Phoenix, Arizona", "London, England");

        return list;
    }
    public ObservableList getContact(){
        String sqlStmt = "SELECT userName FROM user;";
        QueryController.makeQuery(sqlStmt);
        ResultSet results = QueryController.getResult();
 
         ObservableList<String> list = FXCollections.observableArrayList();
        try{
            while(results.next()){
                list.add(results.getString("userName"));
            }
        }catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        return list;   
    }
    public ObservableList getType(){
        ObservableList<String> type = FXCollections.observableArrayList();
        type.addAll("Meeting", "Consulting");
        return type;
    }
    public ObservableList getAvailableTimes(LocalDate date, User contact){        
        ObservableList<String> availableTime = FXCollections.observableArrayList();
        ResultSet results = QueryController.getAppointmentTime(date, contact.getUserName());

        ZoneId zone = ZoneId.systemDefault();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
 
        // Lambda stream to fill in Operation hours of 8 AM to 6 PM
        // Cuts the need to create interation variables and nested loops
        IntStream.rangeClosed(8, 17).forEach((n)->{
            availableTime.add(ZonedDateTime.
                of(LocalDateTime.of(date, LocalTime.of(n, 0)), zone).format(format));
            availableTime.add(ZonedDateTime.
                of(LocalDateTime.of(date, LocalTime.of(n, 15)), zone).format(format));
            availableTime.add(ZonedDateTime.
                of(LocalDateTime.of(date, LocalTime.of(n, 30)), zone).format(format));
            availableTime.add(ZonedDateTime.
                of(LocalDateTime.of(date, LocalTime.of(n, 45)), zone).format(format));
        });
        try{
            while(results.next()){
                // Get time from database and set time zone
                ZonedDateTime start = ZonedDateTime.of
                (results.getTimestamp("start").toLocalDateTime(), ZoneId.of(dataBaseZone));
                
                ZonedDateTime end = ZonedDateTime.of
                (results.getTimestamp("end").toLocalDateTime(), ZoneId.of(dataBaseZone));
                
                // Convert from database time zone before compairing
                start = start.withZoneSameInstant(ZoneId.systemDefault());
                end = end.withZoneSameInstant(ZoneId.systemDefault());
                
                if(availableTime.contains(start.format(format))){
                    int startIndex = availableTime.indexOf(start.format(format));
                    int endIndex = availableTime.indexOf(end.format(format));
                    
                    IntStream.rangeClosed(startIndex, endIndex).forEach((n)->{
                        availableTime.remove(startIndex);});
                }
            }
        }catch(SQLException e){
            
        }
        return availableTime; 
    }
    public ObservableList getAvailableLength(String start, ObservableList<String> times){
        ObservableList<Integer> length = FXCollections.observableArrayList();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime checkStart = LocalTime.parse(start, format);       
        Stream<Integer> choices = Stream.of(15,30,45,60);             
        /*
            Stream lambda used to parse available time list to calculate 
            the available appointment lengths.
          
        */ 
        choices.filter(n -> (times.contains(checkStart.plusMinutes(n).format(format)))).forEach(n->{
            length.add(n);
    });
        if((length.size() > 0) && (length.get(0) != 15)){
            length.clear();
        }
        return length;
    }
}
