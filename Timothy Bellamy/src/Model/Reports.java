/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author TJ
 */
public class Reports {


    
    private SimpleStringProperty month = new SimpleStringProperty();
    private SimpleStringProperty appointmentCount = new SimpleStringProperty();
    private SimpleStringProperty customerName = new SimpleStringProperty();
    private SimpleStringProperty type = new SimpleStringProperty();
    private SimpleStringProperty start = new SimpleStringProperty();
    private SimpleStringProperty end = new SimpleStringProperty();
    private SimpleStringProperty date = new SimpleStringProperty();
    private SimpleStringProperty city = new SimpleStringProperty();
    private SimpleStringProperty cityCount = new SimpleStringProperty();
    
    
    public Reports(){}
    // Property getters
    public StringProperty getMonth(){return month;}
    public StringProperty getAppointmentCount(){return appointmentCount;}
    public StringProperty getCustomerName(){return customerName;}
    public StringProperty getType(){return type;}
    public StringProperty getStart(){return start;}
    public StringProperty getEnd(){return end;}
    public StringProperty getDate(){return date;}
    public StringProperty getCity(){return city;}
    public StringProperty getCityCount(){return cityCount;}
     
    // Setters
    public void setMonth(String value){month.set(value);}
    public void setAppointmentCount(String value){appointmentCount.set(value);}
    public void setCustomerName(String value){customerName.set(value);}
    public void setType(String value){type.set(value);}
    public void setStart(String value){start.set(value);}
    public void setEnd(String value){end.set(value);}
    public void setDate(String value){date.set(value);}
    public void setCity(String value){city.set(value);}
    public void setCityCount(String value){cityCount.set(value);}
    
    
    
}
