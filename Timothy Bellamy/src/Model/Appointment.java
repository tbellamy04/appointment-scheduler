/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author TJ
 */
public class Appointment {
    // Set in database can not be changed in gui
    private SimpleIntegerProperty _id = new SimpleIntegerProperty();
    // Used only for database queries not changed in gui
    private int _custId;
    
    private SimpleStringProperty _title = new SimpleStringProperty();
    private SimpleStringProperty _description = new SimpleStringProperty();
    private SimpleStringProperty _location = new SimpleStringProperty();
    private SimpleStringProperty _contact = new SimpleStringProperty();
    private SimpleStringProperty _type = new SimpleStringProperty();
    private SimpleStringProperty _url = new SimpleStringProperty();
    private ZonedDateTime _start;
    private ZonedDateTime _end;
    private ObjectProperty<LocalDate> _date = new SimpleObjectProperty<>();
    
    private DateTimeFormatter format = DateTimeFormatter.ofPattern("hh:mm a z");

    public Appointment
        (int id, int custId, String title, String description, String location,
         String contact, String type, String url, ZonedDateTime start, ZonedDateTime end) {
            
            _id.set(id);
            _custId = custId;
            _title.set(title);
            _description.set(description);
            _location.set(location);
            _contact.set(contact);
            _type.set(type);
            _url.set(url);
            _start = start;
            _end = end;
            _date.set(start.toLocalDate());        
    }
    // FX Properties
    public StringProperty getTitleProperty(){
        return _title;
    }
    public StringProperty getDescriptionProperty(){
        return _description;
    }
    public StringProperty getLocationProperty(){
        return _location;
    }
    public StringProperty getContactProperty(){
        return _contact;
    }
    public StringProperty getStartProperty(){       
        SimpleStringProperty s = new SimpleStringProperty();    
        s.set(_start.format(format));
        return s;
    }
    public StringProperty getEndProperty(){
        SimpleStringProperty e = new SimpleStringProperty();    
        e.set(_end.format(format));
        return e;
    }
    public ObjectProperty getDateProperty(){
        return _date;
    }
    
    //Getters
    public int getId(){
        return _id.get();
    }
    public int getCustId(){
        return _custId;
    }
    public String getTitle(){
        return _title.get();
    }
    public String getDescription(){
        return _description.get();
    }
    public String getLocation(){
        return _location.get();
    }
    public String getContact(){
        return _contact.get();
    }
    public String getType(){
        return _type.get();
    }
    public String getUrl(){
        return _url.get();
    }
    public ZonedDateTime getStart(){
        return _start;
    }
    public ZonedDateTime getEnd(){
        return _end;
    }
    public LocalDate getDate(){
        return _date.get();
    }
    
    //Setters
    public void setTitle(String newTitle){
        _title.set(newTitle);
    }
    public void setDescription(String newDescription){
        _description.set(newDescription);
    }
    public void setLocation(String newLocation){
        _location.set(newLocation);
    }
    public void setContact(String newContact){
        _contact.set(newContact);
    }
    public void setType(String newType){
        _type.set(newType);
    }
    public void setUrl(String newUrl){
        _url.set(newUrl);
    }
    public void setStart(ZonedDateTime start){
        _start = start;
    }
    public void setEnd(ZonedDateTime end){
        _end = end;
    }
    public void setDate(LocalDate date){
        _date.set(date);
    }
}
