/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author TJ
 */
public class User {
    private SimpleIntegerProperty _userId = new SimpleIntegerProperty();
    private SimpleStringProperty _userName = new SimpleStringProperty();
    
    public User(int id, String name){
        _userId.set(id);
        _userName.set(name);
    }
    
    public int getUserId(){
        return _userId.get();
    }
    public String getUserName(){
        return _userName.get();
    }
    
    @Override
    public String toString(){
         return String.format(getUserName());
        }
}
