/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Model;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author TJ
 */
public class Customer {
    private SimpleIntegerProperty _id = new SimpleIntegerProperty();
    private SimpleStringProperty _name = new SimpleStringProperty();
    private SimpleStringProperty _address = new SimpleStringProperty();
    private SimpleStringProperty _address2 = new SimpleStringProperty();
    private SimpleStringProperty _city = new SimpleStringProperty();
    private SimpleStringProperty _country = new SimpleStringProperty();
    private SimpleStringProperty _zip = new SimpleStringProperty();
    private SimpleStringProperty _phone = new SimpleStringProperty();
    
    public Customer
        (int id, String name, String address, String address2, String city,
        String country, String zip, String phone){
            _id.set(id);
            _name.set(name);
            _address.set(address);
            _address2.set(address2);
            _city.set(city);
            _country.set(country);
            _zip.set(zip);
            _phone.set(phone);
        }
    //Getters
    public int getId(){
        return _id.get();
    }
    public String getName(){
        return _name.get();
    }
    public String getAddress(){
        return _address.get();
    }
    public String getAddress2(){
        return _address2.get();
    }
    public String getCity(){
        return _city.get();
    }
    public String getCountry(){
        return _country.get();
    }
    public String getZip(){
        return _zip.get();
    }
    public String getPhone(){
        return _phone.get();
    }
    //Setters
    public void setName(String newName){
        _name.set(newName);
    }
    public void setAddress(String newAddress){
        _address.set(newAddress);
    }
    public void setAddress2(String newAddress2){
        _address2.set(newAddress2);
    }
    public void setCity(String newCity){
        _city.set(newCity);
    }
    public void setCountry(String newCountry){
        _country.set(newCountry);
    }
    public void setZip(String newZip){
        _zip.set(newZip);
    }
    public void setPhone(String newPhone){
        _phone.set(newPhone);
    }
    
        // Overides to_string() so Customer name is used
        @Override
        public String toString(){
            return String.format(getName());
        }
    }
