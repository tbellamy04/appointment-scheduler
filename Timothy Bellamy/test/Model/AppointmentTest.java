///*
// * Name: Timothy Bellamy
// * ID: 000923380
// */
//package Model;
//
//import java.sql.Time;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.util.Date;
//import javafx.beans.property.SimpleStringProperty;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author TJ
// */
//public class AppointmentTest {
//    
//    Appointment appointment;
//    
//    // Time variables used for testing
//    LocalTime time = LocalTime.now();
//    int hour = time.getHour();
//    int min = time.getMinute();
//    Date date = new Date();
//    LocalTime testTime = new LocalDateTime;
//    
//    public AppointmentTest() {
//        appointment = new Appointment
//            (1,1,"Title", "Description", "Location",
//             "Contact", "Type", "Url",testTime, testTime,
//                date);
//    }
//    @Test
//    public void getId() {
//        assertEquals(1, appointment.getId());
//    }
//    @Test
//    public void getCustId(){
//        assertEquals(1, appointment.getCustId());
//    }
//    @Test
//    public void getTitle(){
//        assertEquals("Title", appointment.getTitle());
//    }
//    @Test
//    public void getDescription(){ 
//        assertEquals("Description", appointment.getDescription());
//    }
//    @Test
//    public void getLocation(){
//        assertEquals("Location", appointment.getLocation());
//    }
//    @Test
//    public void getContact(){
//        assertEquals("Contact", appointment.getContact());
//    }
//    @Test
//    public void getType(){
//        assertEquals("Type", appointment.getType());
//    }
//    @Test
//    public void getUrl(){
//        assertEquals("Url", appointment.getUrl());
//    }
//    @Test
//    public void getStart(){
//        assertEquals(testTime, appointment.getStart());
//    }
//    @Test
//    public void getEnd(){
//        assertEquals(testTime, appointment.getEnd());
//    }
//    @Test
//    public void getDate(){
//        assertEquals(date, appointment.getDate());
//    }
//    @Test
//    public void setTitle(){
//        appointment.setTitle("Test");
//        assertEquals("Test", appointment.getTitle());
//    }
//    @Test
//    public void setDescription(){
//        appointment.setDescription("Test");
//        assertEquals("Test", appointment.getDescription());
//    }
//    @Test
//    public void setLocation(){
//        appointment.setLocation("Test");
//        assertEquals("Test", appointment.getLocation());
//    }
//    @Test
//    public void setContact(){
//        appointment.setContact("Test");
//        assertEquals("Test", appointment.getContact());
//    }
//    @Test
//    public void setType(){
//        appointment.setType("Test");
//        assertEquals("Test", appointment.getType());
//    }
//    @Test
//    public void setUrl(){
//        appointment.setUrl("Test");
//        assertEquals("Test", appointment.getUrl());
//    }
//    @Test
//    public void setStart(){
//        LocalTime t = LocalTime.now();
//        int hr = time.getHour();
//        int minute = time.getMinute();
//        Time currentTime = new Time(hr, minute,0);
//        appointment.setStart(currentTime);
//        
//        assertEquals(currentTime, appointment.getStart());
//    }
//    @Test
//    public void setend(){
//        LocalTime t = LocalTime.now();
//        int hr = time.getHour();
//        int minute = time.getMinute();
//        Time currentTime = new Time(hr, minute,0);
//        appointment.setEnd(currentTime);
//        
//        assertEquals(currentTime, appointment.getEnd());        
//    }
//    @Test
//    public void setDate(){
//        date.setDate(8);
//        appointment.setDate(date);
//        assertEquals(date, appointment.getDate());
//    }
//}
