/*
 * Name: Timothy Bellamy
 * ID: 000923380
 */
package Model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TJ
 */
public class CustomerTest {
    
    Customer customer;
    
    public CustomerTest() {
     
        customer = new Customer
            (1, "Name", "Address", "Address2", "City", "Country",
             "Zip", "Phone");
    }

    @Test
    public void getId() {
        assertEquals(1, customer.getId());
    }
    @Test
    public void getName(){
        assertEquals("Name", customer.getName());
    }
    @Test
    public void getAddress(){
        assertEquals("Address", customer.getAddress());
    }
    @Test
    public void getAddress2(){
        assertEquals("Address2", customer.getAddress2());
    }
    @Test
    public void getCity(){
        assertEquals("City", customer.getCity());
    }
    @Test
    public void getCountry(){
        assertEquals("Country", customer.getCountry());
    }
    @Test
    public void getZip(){
        assertEquals("Zip", customer.getZip());
    }
    @Test
    public void getPhone(){
        assertEquals("Phone", customer.getPhone());
    }
    @Test
    public void setName(){
        customer.setName("Test");
        assertEquals("Test", customer.getName());
    }
    @Test
    public void setAddress(){
        customer.setAddress("Test");
        assertEquals("Test", customer.getAddress());
    }
    @Test
    public void setAddress2(){
        customer.setAddress2("Test");
        assertEquals("Test", customer.getAddress2());
    }
    @Test
    public void setCity(){
        customer.setCity("Test");
        assertEquals("Test", customer.getCity());
    }
    @Test
    public void setCountry(){
        customer.setCountry("Test");
        assertEquals("Test", customer.getCountry());
    }
    @Test
    public void setZip(){
        customer.setZip("Test");
        assertEquals("Test", customer.getZip());
    }
    @Test
    public void setPhone(){
        customer.setPhone("Test");
        assertEquals("Test", customer.getPhone());
    }
}
